package com.example.Conformite.client.repositories;


import com.example.Conformite.client.entities.Pers_morale_be;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Persm_be_repository extends JpaRepository<Pers_morale_be,Long> {
}
