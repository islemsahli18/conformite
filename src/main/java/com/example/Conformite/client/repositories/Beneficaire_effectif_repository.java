package com.example.Conformite.client.repositories;

import com.example.Conformite.client.entities.Beneficaire_effectif;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Beneficaire_effectif_repository extends JpaRepository<Beneficaire_effectif,Long> {
}
