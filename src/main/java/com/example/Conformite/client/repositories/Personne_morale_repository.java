package com.example.Conformite.client.repositories;

import com.example.Conformite.client.entities.Personne_morale;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Personne_morale_repository extends JpaRepository<Personne_morale,Long> {
}
