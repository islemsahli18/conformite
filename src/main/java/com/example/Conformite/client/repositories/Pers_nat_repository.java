package com.example.Conformite.client.repositories;

import com.example.Conformite.client.entities.Pers_nat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Pers_nat_repository extends JpaRepository<Pers_nat,Long> {
}
