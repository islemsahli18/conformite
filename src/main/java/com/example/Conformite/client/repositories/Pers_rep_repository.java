package com.example.Conformite.client.repositories;

import com.example.Conformite.client.entities.Pers_morale_rep;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Pers_rep_repository extends JpaRepository<Pers_morale_rep,Long> {
}
