package com.example.Conformite.client.repositories;

import com.example.Conformite.client.entities.Personne_physique;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Personne_physique_repository extends JpaRepository<Personne_physique,Long> {
}
