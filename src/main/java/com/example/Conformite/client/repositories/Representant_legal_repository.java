package com.example.Conformite.client.repositories;

import com.example.Conformite.client.entities.Representant_legal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Representant_legal_repository extends JpaRepository<Representant_legal,Long> {
}
