package com.example.Conformite.client.repositories;

import com.example.Conformite.client.entities.Pers_par;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Pers_par_repository extends JpaRepository<Pers_par,Long> {
}
