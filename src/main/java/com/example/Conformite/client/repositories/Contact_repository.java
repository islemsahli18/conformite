package com.example.Conformite.client.repositories;


import com.example.Conformite.client.entities.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Contact_repository extends JpaRepository<Contact,Long> {
}
