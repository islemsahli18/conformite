package com.example.Conformite.client.repositories;

import com.example.Conformite.client.entities.Parente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Parente_repository extends JpaRepository<Parente,Long> {
}
