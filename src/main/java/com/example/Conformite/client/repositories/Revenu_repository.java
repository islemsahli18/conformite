package com.example.Conformite.client.repositories;

import com.example.Conformite.client.entities.Revenu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Revenu_repository extends JpaRepository<Revenu,Long> {
}
