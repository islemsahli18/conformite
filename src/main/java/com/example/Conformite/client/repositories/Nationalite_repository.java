package com.example.Conformite.client.repositories;

import com.example.Conformite.client.entities.Nationalite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Nationalite_repository extends JpaRepository<Nationalite,Long> {
}
