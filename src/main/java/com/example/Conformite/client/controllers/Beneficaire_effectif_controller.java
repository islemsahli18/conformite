package com.example.Conformite.client.controllers;

import com.example.Conformite.client.entities.Beneficaire_effectif;
import com.example.Conformite.client.services.Beneficaire_effectif_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value="/BE",method= {RequestMethod.GET})
public class Beneficaire_effectif_controller {
    @Autowired
    Beneficaire_effectif_service BEService;
    @PostMapping("/Create")
    public String createCours(@Validated @RequestBody Beneficaire_effectif BE) {
        BEService.saveBF(BE);
        return "BF cree";
    }
    @GetMapping("/GetAll")
    public List<Beneficaire_effectif> getAllCours(){
        return BEService.listBF();
    }
    @GetMapping("/GetBE/{id}")
    public Optional<Beneficaire_effectif> findCours(@PathVariable Long id){
      /*Optional<Cours> cours=coursService.findCours(id);
      if(cours==null) throw new
      CoursIntrouvableException("Le cours avec le code "+id+"est introuvable");*/
        return BEService.findBF(id);
    }
    @PutMapping("/Update/{id}")
    public String UpdateCours(@Validated @RequestBody Beneficaire_effectif cours, @PathVariable Long id){
        BEService.updateBF(id,cours);
        return"La mise a jour a ete faite avec succees";
    }
    @DeleteMapping(value="/DeleteBE/{id}")
    public String deleteCours(@PathVariable String id){
        BEService.removeBF(Long.parseLong(id));
        return "BE supprimee avec succee";
    }
}
