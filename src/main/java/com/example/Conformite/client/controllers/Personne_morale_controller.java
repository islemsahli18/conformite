package com.example.Conformite.client.controllers;

import com.example.Conformite.client.entities.Personne_morale;
import com.example.Conformite.client.services.Personne_morale_service;
import org.springframework.beans.factory.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(value="/PersonneMorale",method= {RequestMethod.GET})

public class Personne_morale_controller {
    @Autowired
    Personne_morale_service PM_S;


    @PostMapping("/Create")
    public String createPersonne_Mrl(@Validated @RequestBody Personne_morale PR) {
        PM_S.savePersonne_mrl(PR);
        return "Personne Morale Cree";
    }

    @DeleteMapping(value="/Delete/{id}")
    public String deletePersonne_Mrl(@PathVariable String id){
        PM_S.removePersonne_mrl(Long.parseLong(id));
        return "Personne Morale supprimee avec succees";
    }

    @GetMapping("/Get/{id}")
    public Optional<Personne_morale> findPersonne_Mrl(@PathVariable Long id){

        return  PM_S.findPersonne_mrl(id);

    }

    @GetMapping ("/GetAll")
    public List<Personne_morale> getAll_Personne_mrl(){
        return PM_S.listPersonne_mrl();}

    @PutMapping("/Update/{id}")
    public String UpdatePersonne_Mrl(@PathVariable Long id,@Validated @RequestBody Personne_morale PM)
    {PM.setCode_clt(id);
        PM_S.updatePersonne_mrl(PM);
        return"success:Update a ete bien faite";
    }


}
