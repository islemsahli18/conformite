package com.example.Conformite.client.controllers;


import com.example.Conformite.client.entities.Pers_morale_rep;
import com.example.Conformite.client.services.Pers_morale_rep_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/Pers_rep",method= {RequestMethod.GET})
public class Pers_morale_rep_controller {



        @Autowired
        Pers_morale_rep_service parService;
        @PostMapping("/Create")
        public String create_per_nat(@Validated @RequestBody Pers_morale_rep par) {
            parService.savePR(par);
            return "creation est faite ";
        }
        @GetMapping("/GetAll")
        public List<Pers_morale_rep> getAllnat(){
            return parService.listPR();
        }
        @GetMapping("/Get/{id}")
        public Optional<Pers_morale_rep> findnat(@PathVariable Long id){
      /*Optional<Cours> cours=coursService.findCours(id);
      if(cours==null) throw new
      CoursIntrouvableException("Le cours avec le code "+id+"est introuvable");*/
            return parService.findPR(id);
        }
        @PutMapping("/Update/{id}")
        public String Updatenat(@Validated @RequestBody Pers_morale_rep par, @PathVariable Long id){
            parService.updatePR(id,par);
            return"La mise a jour a ete faite avec succees";
        }
        @DeleteMapping(value="/Delete/{id}")
        public String deletenat(@PathVariable String id){
            parService.removePR(Long.parseLong(id));
            return "supprimee avec succee";
        }
    }



