package com.example.Conformite.client.controllers;


import com.example.Conformite.client.entities.Personne_physique;
import com.example.Conformite.client.services.Personne_physique_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/PersonnePhysique",method= {RequestMethod.GET})
public class Personne_physique_controller {
    @Autowired
    Personne_physique_service PP_S;

    @PostMapping("/Create")
    public String createPersonne_Phy(@Validated @RequestBody Personne_physique PP) {

        PP_S.updatePersonne_phy(PP);
        PP_S.savePersonne_phy(PP);
        return "Personne physique est cree avec succees";
    }

    @DeleteMapping(value="/Delete/{id}")
    public String deletePersonne_Phy(@PathVariable String id){
        PP_S.removePersonne_phy(Long.parseLong(id));
        return "Personne physique est supprimee avec succees";
    }

    @GetMapping("/Get/{id}")
    public Optional<Personne_physique> findPersonne_Phy(@PathVariable Long id){
        return  PP_S.findPersonne_phy(id);
    }

    @GetMapping ("/GetAll")
    public List<Personne_physique> getAll_Personne_phy(){
        return PP_S.listPersonne_phy();}

    @PutMapping("/Update/{id}")
    public String UpdatePersonne_Mrl(@PathVariable Long id,@Validated @RequestBody Personne_physique PP)
    {PP.setCode_clt(id);
        PP_S.updatePersonne_phy(PP);
        return"success:Update a ete bien faite";
    }
}
