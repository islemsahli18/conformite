package com.example.Conformite.client.controllers;

import com.example.Conformite.client.entities.Nationalite;
import com.example.Conformite.client.services.Nationalite_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/nationalite",method= {RequestMethod.GET})

public class Nationalite_controller {
    @Autowired
    Nationalite_service natService;
    @PostMapping("/Create")
    public String createnat(@Validated @RequestBody Nationalite nat) {
        natService.saveNat(nat);
        return "creation est faite ";
    }
    @GetMapping("/GetAll")
    public List<Nationalite> getAllnat(){
        return natService.listNat();
    }
    @GetMapping("/Getnat/{id}")
    public Optional<Nationalite> findnat(@PathVariable Long id){
      /*Optional<Cours> cours=coursService.findCours(id);
      if(cours==null) throw new
      CoursIntrouvableException("Le cours avec le code "+id+"est introuvable");*/
        return natService.findNat(id);
    }
    @PutMapping("/Update/{id}")
    public String Updatenat(@Validated @RequestBody Nationalite nat, @PathVariable Long id){
        natService.updateNat(id,nat);
        return"La mise a jour a ete faite avec succees";
    }
    @DeleteMapping(value="/Delete/{id}")
    public String deletenat(@PathVariable String id){
        natService.removeNat(Long.parseLong(id));
        return "supprimee avec succee";
    }


}
