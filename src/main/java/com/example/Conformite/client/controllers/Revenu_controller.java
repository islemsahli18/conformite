package com.example.Conformite.client.controllers;

import com.example.Conformite.client.entities.Revenu;
import com.example.Conformite.client.services.Revenu_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/Revenu",method= {RequestMethod.GET})
public class Revenu_controller {

    @Autowired
    Revenu_service R_S;


    @PostMapping("/Create")
    public String createRevenu_service(@Validated @RequestBody Revenu R) {
        R_S.saveRevenu(R);
        return "Creation done";
    }

    @DeleteMapping(value="/Delete/{id}")
    public String deleteRevenu_service(@PathVariable String id){
        R_S.removeRevenu(Long.parseLong(id));
        return "Done";
    }

    @GetMapping("/Get/{id}")
    public Optional<Revenu> findRevenu_service(@PathVariable Long id){
        return  R_S.findRevenu(id);
    }

    @GetMapping ("/GetAll")
    public List<Revenu> getAllRevenu(){
        return R_S.listRevenu();}

    @PutMapping("/Update/{id}")
    public String UpdateRevenu(@PathVariable Long id,@Validated @RequestBody Revenu R)
    {R.setID_R(id);
        R_S.updateRevenu(R);
        return"success:Update a ete bien faite";
    }
}
