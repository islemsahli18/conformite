package com.example.Conformite.client.controllers;

import com.example.Conformite.client.entities.Pers_morale_be;
import com.example.Conformite.client.services.Pers_morale_be_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value="/Persm_be",method= {RequestMethod.GET})
public class Pers_morale_be_controller {

        @Autowired
        Pers_morale_be_service parService;
        @PostMapping("/Create")
        public String create_per_nat(@Validated @RequestBody Pers_morale_be par) {
            parService.savePBE(par);
            return "creation est faite ";
        }
        @GetMapping("/GetAll")
        public List<Pers_morale_be> getAllnat(){
            return parService.listPBE();
        }
        @GetMapping("/Get/{id}")
        public Optional<Pers_morale_be> findnat(@PathVariable Long id){
      /*Optional<Cours> cours=coursService.findCours(id);
      if(cours==null) throw new
      CoursIntrouvableException("Le cours avec le code "+id+"est introuvable");*/
            return parService.findPBE(id);
        }
        @PutMapping("/Update/{id}")
        public String Updatenat(@Validated @RequestBody Pers_morale_be par, @PathVariable Long id){
            parService.updatePBE(id,par);
            return"La mise a jour a ete faite avec succees";
        }
        @DeleteMapping(value="/Delete/{id}")
        public String deletenat(@PathVariable String id){
            parService.removePBE(Long.parseLong(id));
            return "supprimee avec succee";
        }
    }

