package com.example.Conformite.client.controllers;

import com.example.Conformite.client.entities.Parente;
import com.example.Conformite.client.services.Parente_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value="/parente",method= {RequestMethod.GET})
public class Parente_controller {
    @Autowired
    Parente_service parService;
    @PostMapping("/Create")
    public String createnat(@Validated @RequestBody Parente par) {
        parService.savePar(par);
        return "creation est faite ";
    }
    @GetMapping("/GetAll")
    public List<Parente> getAllnat(){
        return parService.listPar();
    }
    @GetMapping("/Get/{id}")
    public Optional<Parente> findnat(@PathVariable Long id){
      /*Optional<Cours> cours=coursService.findCours(id);
      if(cours==null) throw new
      CoursIntrouvableException("Le cours avec le code "+id+"est introuvable");*/
        return parService.findPar(id);
    }
    @PutMapping("/Update/{id}")
    public String Updatenat(@Validated @RequestBody Parente par, @PathVariable Long id){
        parService.updatePar(id,par);
        return"La mise a jour a ete faite avec succees";
    }
    @DeleteMapping(value="/Delete/{id}")
    public String deletenat(@PathVariable String id){
        parService.removePar(Long.parseLong(id));
        return "supprimee avec succee";
    }
}
