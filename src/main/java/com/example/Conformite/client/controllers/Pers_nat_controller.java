package com.example.Conformite.client.controllers;

import com.example.Conformite.client.entities.Pers_nat;
import com.example.Conformite.client.services.Pers_nat_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/Pers_Nat",method= {RequestMethod.GET})

public class Pers_nat_controller {

        @Autowired
        Pers_nat_service parService;
        @PostMapping("/Create")
        public String create_per_nat(@Validated @RequestBody Pers_nat par) {
            parService.savePN(par);
            return "creation est faite ";
        }
        @GetMapping("/GetAll")
        public List<Pers_nat> getAllnat(){
            return parService.listPN();
        }
        @GetMapping("/Get/{id}")
        public Optional<Pers_nat> findnat(@PathVariable Long id){
      /*Optional<Cours> cours=coursService.findCours(id);
      if(cours==null) throw new
      CoursIntrouvableException("Le cours avec le code "+id+"est introuvable");*/
            return parService.findPN(id);
        }
        @PutMapping("/Update/{id}")
        public String Updatenat(@Validated @RequestBody Pers_nat par, @PathVariable Long id){
            parService.updatePN(id,par);
            return"La mise a jour a ete faite avec succees";
        }
        @DeleteMapping(value="/Delete/{id}")
        public String deletenat(@PathVariable String id){
            parService.removePN(Long.parseLong(id));
            return "supprimee avec succee";
        }
    }


