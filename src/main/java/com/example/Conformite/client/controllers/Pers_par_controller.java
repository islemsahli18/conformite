package com.example.Conformite.client.controllers;

import com.example.Conformite.client.entities.Pers_par;
import com.example.Conformite.client.services.Pers_par_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value="/Pers_par",method= {RequestMethod.GET})
public class Pers_par_controller {

        @Autowired
        Pers_par_service parService;
        @PostMapping("/Create")
        public String create_per_nat(@Validated @RequestBody Pers_par par) {
            parService.savePS(par);
            return "creation est faite ";
        }
        @GetMapping("/GetAll")
        public List<Pers_par > getAllnat(){
            return parService.listPS();
        }
        @GetMapping("/Get/{id}")
        public Optional<Pers_par > findnat(@PathVariable Long id){
      /*Optional<Cours> cours=coursService.findCours(id);
      if(cours==null) throw new
      CoursIntrouvableException("Le cours avec le code "+id+"est introuvable");*/
            return parService.findPS(id);
        }
        @PutMapping("/Update/{id}")
        public String Updatenat(@Validated @RequestBody Pers_par  par,@PathVariable Long id){
            parService.updatePS(id,par);
            return"La mise a jour a ete faite avec succees";
        }
        @DeleteMapping(value="/Delete/{id}")
        public String deletenat(@PathVariable String id){
            parService.removePS(Long.parseLong(id));
            return "supprimee avec succee";
        }
    }

