package com.example.Conformite.client.controllers;


import com.example.Conformite.client.entities.Representant_legal;
import com.example.Conformite.client.services.Representant_legal_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value="/Representant_legal",method= {RequestMethod.GET})
public class Representant_legal_controller {
    @Autowired
    Representant_legal_service RL_S;


    @PostMapping("/Create")
    public String createRepresentant_legal(@Validated @RequestBody Representant_legal RL) {
        RL_S.saveRepresentant_legal(RL);
        return "Creation done";
    }

    @DeleteMapping(value="/Delete/{id}")
    public String deleterepresentant_legal(@PathVariable String id){
        RL_S.removeRepresentant_legal(Long.parseLong(id));
        return "Done";
    }

    @GetMapping("/Get/{id}")
    public Optional<Representant_legal> findRepresentant_legal(@PathVariable Long id){

        return  RL_S.findRepresentant_legal(id);

    }

    @GetMapping ("/GetAll")
    public List<Representant_legal> getAllRepresentant_legal(){
        return RL_S.listRepresentant_legal();}

    @PutMapping("/Update/{id}")
    public String UpdateRepresentant_legal(@PathVariable Long id,@Validated @RequestBody Representant_legal RL)
    {RL.setCode_clt(id);
        RL_S.updateRepresentant_legal(RL);
        return"success:Update a ete bien faite";
    }
}
