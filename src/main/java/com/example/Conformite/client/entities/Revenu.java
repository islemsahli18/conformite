package com.example.Conformite.client.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.sql.Date;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Revenu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long ID_R;
    private  String Nature;
    private  float Montant;
    private  String Devise;
    private Date Date_R;


    //Revenu many to one  entre revenu et personne physique
    @ManyToOne
    @JoinColumn(name="Code_pers")
    private Personne_physique pers;

    //GETTERS AND SETTERS
    public String getNature() {
        return Nature;
    }
    public void setNature(String nature) {
        Nature = nature;
    }
    public float getMontant() {
        return Montant;
    }
    public void setMontant(float montant) {
        Montant = montant;
    }
    public String getDevise() {
        return Devise;
    }
    public void setDevise(String devise) {
        Devise = devise;
    }
    public Date getDate_R() {
        return Date_R;
    }
    public void setDate_R(Date date_R) {
        Date_R = date_R;
    }
    public Long getID_R() {
        return ID_R;
    }
    public void setID_R(Long ID_R) {
        this.ID_R = ID_R;
    }

}
