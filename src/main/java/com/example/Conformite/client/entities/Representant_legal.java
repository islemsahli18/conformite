package com.example.Conformite.client.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Representant_legal extends Personne_physique {

    private String fonction_organisme ;

    //Relation entre Pers_morale_rep et representant legal:
    @OneToMany(mappedBy = "rep",cascade = CascadeType.ALL)
    private List<Pers_morale_rep> ListPers_rep;

    //getter and setter
    public void setFonction_organisme(String fonction_organisme) {
        this.fonction_organisme = fonction_organisme;
    }

    public String getFonction_organisme() {
        return fonction_organisme;
    }






}
