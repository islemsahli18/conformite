package com.example.Conformite.client.entities;


import com.example.Conformite.PEP.entities.PEP;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Contact {
    private static final long serialVersionUID=1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long code_clt;
    private  String pays_residence;
    private  String email;
    private  String num_telephone;
    private  boolean code_TIN ;
    private String adresse;

    //Relations entre PEP et contact
    @OneToOne(mappedBy ="contact",cascade = CascadeType.ALL)
    private PEP pep;




    //Constructeurs
    public Contact(Long code_clt){
        this.code_clt = code_clt;
    }

    public Contact(Long code_clt, String pays_residence, String email, String num_telephone, boolean code_tin, String adresse) {
    }


    //Getters et Setters
    public Long getCode_clt() {
        return code_clt;
    }

    public String getPays_residence() {
        return pays_residence;
    }

    public String getEmail() {
        return email;
    }

    public String getNum_telephone() {
        return num_telephone;
    }

    public boolean isCode_TIN() {
        return code_TIN;
    }

    public String getAdresse() {
        return adresse;
    }


    public void setCode_clt(Long code_clt) {
        code_clt = code_clt;
    }

    public void setPays_residence(String pays_residence) {
        pays_residence = pays_residence;
    }

    public void setEmail(String email) {
        email = email;
    }

    public void setNum_telephone(String num_telephone) {
        num_telephone = num_telephone;
    }

    public void setCode_TIN(boolean code_TIN) {
        code_TIN = code_TIN;
    }

    public void setAdresse(String adresse) {
        adresse = adresse;
    }

    public void setPep(PEP pep) {
        this.pep = pep;
    }




}

