package com.example.Conformite.client.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Beneficaire_effectif extends Personne_physique {

    private String raison_sociale;
    private float pourcentage;

    //Relation entre pers_rep et representant legal:
    @OneToMany(mappedBy = "b_e",cascade = CascadeType.ALL)
    private List<Pers_morale_be> ListPers_be;



     //GETTERS AND SETTERS
    public String getRaison_sociale() {
        return raison_sociale;
    }

    public void setRaison_sociale(String raison_sociale) {
        this.raison_sociale = raison_sociale;
    }

    public float getPourcentage() {
        return pourcentage;
    }

    public void setPourcentage(float pourcentage) {
        pourcentage = pourcentage;
    }


}
