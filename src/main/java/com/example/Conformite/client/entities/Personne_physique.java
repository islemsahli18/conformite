package com.example.Conformite.client.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity

public class Personne_physique extends Contact {

    private String Nom;
    private String Prenom;
    private Date Date_naiss;
    private Boolean US_permenant;
    private String Num_passport;
    private String  Num_CIN;
    private String  Etat_civil;
    private String sexe;
    private String C_juridique; //capacite juridique
    private String Cat_sociopro ; // categorie socioprofessionnelle
    private String Profession;
    private String Cat_Employeur;  //Catégorie Employeur
    private String Secteur_travail;
    private String Contrat_Travail;
    private String Date_Tit; //date_Titularisation
    private String Pays_travail;
    private Long Num_RNE;
    private Date Date_Extrait_RNE;
    private String Regime_Fiscal;
    private String Matricule_Fiscal;
    private String Code_Douane;
    private String Aff_Soc; //affiliation social: CNSS/CNRPS
    private Long Num_aff;
    private float devise;
    private String Sit_habitat; //SITUATION D'HABITATION!
    private String Reg_mat; //Régime_matrimonial
    private Boolean  Lien_parenté_PEP;
    private Boolean  Statut_PEP;
    private Boolean  carte_verte;


    //relation entre personne physique et revenu
    @OneToMany(mappedBy = "pers",cascade = CascadeType.ALL)
    private List<Revenu> ListRevenu;
    //Relation entre pers_nat et personne_physique
    @OneToMany(mappedBy = "pers1",cascade = CascadeType.ALL)
    private List<Pers_nat> ListPers_nat1;
    //Relation entre pers_par et personne_physique
    @OneToMany(mappedBy = "pers2",cascade = CascadeType.ALL)
    private List<Pers_par> ListPers_par ;

    //getters and setters
    public String getNom() {
        return Nom;
    }
    public void setNom(String nom) {
        Nom = nom;
    }
    public String getPrenom() {
        return Prenom;
    }
    public void setPrenom(String prenom) {
        Prenom = prenom;
    }
    public Date getDate_naiss() {
        return Date_naiss;
    }
    public void setDate_naiss(Date date_naiss) {
        Date_naiss = date_naiss;
    }
    public void setUS_permenant(Boolean US_permenant) {
        this.US_permenant = US_permenant;
    }
    public String getNum_passport() {
        return Num_passport;
    }
    public void setNum_passport(String num_passport) {
        Num_passport = num_passport;
    }
    public String getNum_CIN() {
        return Num_CIN;
    }
    public void setNum_CIN(String num_CIN) {
        Num_CIN = num_CIN;
    }
    public String getEtat_civil() {
        return Etat_civil;
    }
    public void setEtat_civil(String etat_civil) {
        Etat_civil = etat_civil;
    }
    public void setSexe(String sexe) {
        this.sexe = sexe;
    }
    public String getC_juridique() {
        return C_juridique;
    }
    public void setC_juridique(String c_juridique) {
        C_juridique = c_juridique;
    }
    public String getCat_sociopro() {
        return Cat_sociopro;
    }
    public void setCat_sociopro(String cat_sociopro) {
        Cat_sociopro = cat_sociopro;
    }
    public String getProfession() {
        return Profession;
    }
    public void setProfession(String profession) {
        Profession = profession;
    }
    public String getCat_Employeur() {
        return Cat_Employeur;
    }
    public void setCat_Employeur(String cat_Employeur) {
        Cat_Employeur = cat_Employeur;
    }
    public String getSecteur_travail() {
        return Secteur_travail;
    }
    public void setSecteur_travail(String secteur_travail) {
        Secteur_travail = secteur_travail;
    }
    public String getContrat_Travail() {
        return Contrat_Travail;
    }
    public void setContrat_Travail(String contrat_Travail) {
        Contrat_Travail = contrat_Travail;
    }
    public String getDate_Tit() {
        return Date_Tit;
    }
    public void setDate_Tit(String date_Tit) {
        Date_Tit = date_Tit;
    }
    public String getPays_travail() {
        return Pays_travail;
    }
    public void setPays_travail(String pays_travail) {
        Pays_travail = pays_travail;
    }
    public Long getNum_RNE() {
        return Num_RNE;
    }
    public void setNum_RNE(Long num_RNE) {
        Num_RNE = num_RNE;
    }
    public Date getDate_Extrait_RNE() {
        return Date_Extrait_RNE;
    }
    public void setDate_Extrait_RNE(Date date_Extrait_RNE) {
        Date_Extrait_RNE = date_Extrait_RNE;
    }
    public String getRegime_Fiscal() {
        return Regime_Fiscal;
    }
    public void setRegime_Fiscal(String regime_Fiscal) {
        Regime_Fiscal = regime_Fiscal;
    }
    public String getMatricule_Fiscal() {
        return Matricule_Fiscal;
    }
    public void setMatricule_Fiscal(String matricule_Fiscal) {
        Matricule_Fiscal = matricule_Fiscal;
    }
    public String getCode_Douane() {
        return Code_Douane;
    }
    public void setCode_Douane(String code_Douane) {
        Code_Douane = code_Douane;
    }
    public String getAff_Soc() {
        return Aff_Soc;
    }
    public void setAff_Soc(String aff_Soc) {
        Aff_Soc = aff_Soc;
    }
    public Long getNum_aff() {
        return Num_aff;
    }
    public void setNum_aff(Long num_aff) {
        Num_aff = num_aff;
    }
    public void setDevise(float devise) {
        this.devise = devise;
    }
    public String getSit_habitat() {
        return Sit_habitat;
    }
    public void setSit_habitat(String sit_habitat) {
        Sit_habitat = sit_habitat;
    }
    public String getReg_mat() {
        return Reg_mat;
    }
    public void setReg_mat(String reg_mat) {
        Reg_mat = reg_mat;
    }
    public Boolean getLien_parenté_PEP() {
        return Lien_parenté_PEP;
    }
    public void setLien_parenté_PEP(Boolean lien_parenté_PEP) {
        Lien_parenté_PEP = lien_parenté_PEP;
    }
    public Boolean getStatut_PEP() {
        return Statut_PEP;
    }
    public void setStatut_PEP(Boolean statut_PEP) {
        Statut_PEP = statut_PEP;
    }
    public void setCarte_verte(Boolean carte_verte) {
        this.carte_verte = carte_verte;
    }
    public List<Revenu> getListRevenu() {
        return this.ListRevenu;
    }
    public void setListRevenu(List<Revenu> ListRevenu) {
        this.ListRevenu = ListRevenu;
    }
    public Boolean getUS_permenant() {
        return this.US_permenant;
    }
    public  String getSexe() {
        return this.sexe;
    }
    public float getDevise() {
        return this.devise;
    }
    public Boolean getCarte_verte() {
        return this.carte_verte;
    }
}
