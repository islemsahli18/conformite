package com.example.Conformite.client.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Pers_nat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long code_nat_pers;


    //Revenu many to one  entre pers_nat et personne physique
    @ManyToOne
    @JoinColumn(name="Code_pers")
    private Personne_physique pers1;

    //Revenu many to one  entre pers_nat et nationalite
    @ManyToOne
    @JoinColumn(name="Code_nat")
    private Nationalite nat;

   //GETTERS AND SETTERS
    public Long getCode_nat_pers() {
        return code_nat_pers;
    }

    public void setCode_nat_pers(Long code_nat_pers) {
        code_nat_pers = code_nat_pers;
    }

    public Personne_physique getPers1() {
        return pers1;
    }

    public void setPers1(Personne_physique pers1) {
        this.pers1 = pers1;
    }

    public Nationalite getNat() {
        return nat;
    }

    public void setNat(Nationalite nat) {
        this.nat = nat;
    }
}
