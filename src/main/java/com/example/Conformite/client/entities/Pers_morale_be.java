package com.example.Conformite.client.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Pers_morale_be {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long code_pers_be;

    //Revenu many to one  entre pers_be et personne morale
    @ManyToOne
    @JoinColumn(name="Code_pers_Morale")
    private Personne_morale pers_M1;

    //Revenu many to one  entre pers_rep et representant legale
    @ManyToOne
    @JoinColumn(name="Code_be")
    private Beneficaire_effectif b_e;


    //GETTERS AND SETTERS
    public Long getCode_pers_be() {
        return code_pers_be;
    }

    public void setCode_pers_be(Long code_pers_be) {
        code_pers_be = code_pers_be;
    }

    public Personne_morale getPers_M1() {
        return pers_M1;
    }

    public void setPers_M1(Personne_morale pers_M1) {
        this.pers_M1 = pers_M1;
    }

    public Beneficaire_effectif getB_e() {
        return b_e;
    }

    public void setB_e(Beneficaire_effectif b_e) {
        this.b_e = b_e;
    }
}
