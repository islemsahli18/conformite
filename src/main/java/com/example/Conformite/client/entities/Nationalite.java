package com.example.Conformite.client.entities;

import javax.persistence.*;
import  lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Nationalite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long Code_nat;
    private String nationalite;

    //Getters et Setters :
    public String getNationalite() {
        return nationalite;
    }
    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    //Relation entre pers_nat et nationalite :
    @OneToMany(mappedBy = "nat",cascade = CascadeType.ALL)
    private List<Pers_nat> ListPers_nat2;




}
