package com.example.Conformite.client.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Pers_par {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long Code_pers_par;

    //Revenu many to one  entre pers_nat et personne physique
    @ManyToOne
    @JoinColumn(name="Code_pers")
    private Personne_physique pers2;

    //Revenu many to one  entre pers_par et nationalite
    @ManyToOne
    @JoinColumn(name="Code_par")
    private Parente par;

    //Getters and Setters
    public Long getCode_pers_par() {
        return Code_pers_par;
    }

    public void setCode_pers_par(Long code_pers_par) {
        Code_pers_par = code_pers_par;
    }

    public Personne_physique getPers2() {
        return pers2;
    }

    public void setPers2(Personne_physique pers2) {
        this.pers2 = pers2;
    }

    public Parente getPar() {
        return par;
    }

    public void setPar(Parente par) {
        this.par = par;
    }
}
