package com.example.Conformite.client.entities;
import  lombok.*;
import javax.persistence.*;
import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class Parente extends Personne_physique {

    private String lien_parente;


    //Relation entre pers_par et parente
    @OneToMany(mappedBy = "par",cascade = CascadeType.ALL)
    private List<Pers_par> ListPers_par;



    //GETTERS AND SETTERS

    public String getLien_parente() {
        return lien_parente;
    }

    public void setLien_parente(String lien_parente) {
        this.lien_parente = lien_parente;
    }

    public List<Pers_par> getListPers_par() {
        return ListPers_par;
    }

    public void setListPers_par(List<Pers_par> listPers_par) {
        ListPers_par = listPers_par;
    }
}
