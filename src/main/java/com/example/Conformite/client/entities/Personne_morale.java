package com.example.Conformite.client.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Personne_morale extends Contact {

    private String ds ; //Denomination_sociale
    private String  os ;// Objet_social
    private Long num_id ;
    private Date date_const ;
    private String pays_const;
    private Date date_enreg ;
    private boolean associés_PPE;
    private String nature_Operation;
    private String brèveDescription_NO;
    private String origine_Fonds;
    private Boolean of_t ;
    private String pays_of ;
    private String nature_pm ; //nature personne morale
    private String domaine_act;
    private String ancienneté_pro;
    private String forme_juri;

   //Represntant legal et Pers_morale_rep
    @OneToMany(mappedBy = "pers_M",cascade = CascadeType.ALL)
    private List<Pers_morale_rep> ListRep;

    //Beneficiaire Effectif et Pers_morale_be
    @OneToMany(mappedBy = "pers_M1",cascade = CascadeType.ALL)
    private List<Pers_morale_be> ListBE;

   //CONSTRUCTEUR
    public Personne_morale(Long code_clt, String pays_residence, String email, String num_telephone, boolean code_TIN, String adresse, String DS, String OS, Long num_id, String nat, Date date_const, String pays_const, Date date_enreg, boolean associés_PPE, String nature_Operation, String brèveDescription_NO, String origine_Fonds, Boolean OF_T, String pays_OF, String nature_PM, String domaine_Act, String ancienneté_pro, String forme_juri) {
        super(code_clt, pays_residence, email, num_telephone, code_TIN, adresse);
        this.ds = DS;
        this.os = OS;
        num_id = num_id;
        date_const = date_const;
        pays_const = pays_const;
        date_enreg = date_enreg;
        associés_PPE = associés_PPE;
        nature_Operation = nature_Operation;
        brèveDescription_NO = brèveDescription_NO;
        origine_Fonds = origine_Fonds;
        this.of_t = OF_T;
        pays_OF = pays_OF;
        nature_PM = nature_PM;
        domaine_Act = domaine_Act;
        ancienneté_pro = ancienneté_pro;
        forme_juri = forme_juri;
    }

   //GETTERS AND SETTERS
    public String getDs() {
        return ds;
    }

    public void setDs(String ds) {
        this.ds = ds;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public Long getNum_id() {
        return num_id;
    }

    public void setNum_id(Long num_id) {
        this.num_id = num_id;
    }

    public Date getDate_const() {
        return date_const;
    }

    public void setDate_const(Date date_const) {
        this.date_const = date_const;
    }

    public String getPays_const() {
        return pays_const;
    }

    public void setPays_const(String pays_const) {
        this.pays_const = pays_const;
    }

    public Date getDate_enreg() {
        return date_enreg;
    }

    public void setDate_enreg(Date date_enreg) {
        this.date_enreg = date_enreg;
    }

    public boolean isAssociés_PPE() {
        return associés_PPE;
    }

    public void setAssociés_PPE(boolean associés_PPE) {
        this.associés_PPE = associés_PPE;
    }

    public String getNature_Operation() {
        return nature_Operation;
    }

    public void setNature_Operation(String nature_Operation) {
        this.nature_Operation = nature_Operation;
    }

    public String getBrèveDescription_NO() {
        return brèveDescription_NO;
    }

    public void setBrèveDescription_NO(String brèveDescription_NO) {
        this.brèveDescription_NO = brèveDescription_NO;
    }

    public String getOrigine_Fonds() {
        return origine_Fonds;
    }

    public void setOrigine_Fonds(String origine_Fonds) {
        this.origine_Fonds = origine_Fonds;
    }

    public Boolean getOf_t() {
        return of_t;
    }

    public void setOf_t(Boolean of_t) {
        this.of_t = of_t;
    }

    public String getPays_of() {
        return pays_of;
    }

    public void setPays_of(String pays_of) {
        this.pays_of = pays_of;
    }

    public String getNature_pm() {
        return nature_pm;
    }

    public void setNature_pm(String nature_pm) {
        this.nature_pm = nature_pm;
    }

    public String getDomaine_act() {
        return domaine_act;
    }

    public void setDomaine_act(String domaine_act) {
        this.domaine_act = domaine_act;
    }

    public String getAncienneté_pro() {
        return ancienneté_pro;
    }

    public void setAncienneté_pro(String ancienneté_pro) {
        this.ancienneté_pro = ancienneté_pro;
    }

    public String getForme_juri() {
        return forme_juri;
    }

    public void setForme_juri(String forme_juri) {
        this.forme_juri = forme_juri;
    }

    public List<Pers_morale_rep> getListRep() {
        return ListRep;
    }

    public void setListRep(List<Pers_morale_rep> listRep) {
        ListRep = listRep;
    }

    public List<Pers_morale_be> getListBE() {
        return ListBE;
    }

    public void setListBE(List<Pers_morale_be> listBE) {
        ListBE = listBE;
    }
}
