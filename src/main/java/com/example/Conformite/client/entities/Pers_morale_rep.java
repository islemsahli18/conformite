package com.example.Conformite.client.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Pers_morale_rep {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long Code_pers_rep;

    //Revenu many to one  entre pers_rep et personne morale
    @ManyToOne
    @JoinColumn(name="Code_pers_Morale")
    private Personne_morale pers_M;

    //Revenu many to one  entre pers_rep et representant legale
    @ManyToOne
    @JoinColumn(name="Code_rep")
    private Representant_legal rep;


    //getters and setters
    public Long getCode_pers_rep() {
        return Code_pers_rep;
    }

    public void setCode_pers_rep(Long code_pers_rep) {
        Code_pers_rep = code_pers_rep;
    }

    public Personne_morale getPers_M() {
        return pers_M;
    }

    public void setPers_M(Personne_morale pers_M) {
        this.pers_M = pers_M;
    }

    public Representant_legal getRep() {
        return rep;
    }

    public void setRep(Representant_legal rep) {
        this.rep = rep;
    }
}
