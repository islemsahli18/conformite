package com.example.Conformite.client.servicesImp;

import com.example.Conformite.client.entities.Pers_morale_be;
import com.example.Conformite.client.repositories.Persm_be_repository;
import com.example.Conformite.client.services.Pers_morale_be_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service("Persm_be_serviceImp")
public class Pers_morale_be_serviceImp implements Pers_morale_be_service {

        @Autowired
        Persm_be_repository ParRepository;
        @Override
        public void savePBE(Pers_morale_be Par) {
            ParRepository.save(Par);
        }
        @Override
        public void updatePBE(long codeC, Pers_morale_be new_Par) {
            Pers_morale_be par;
            Optional<Pers_morale_be> OldPar= ParRepository.findById(codeC);
            if(OldPar.isPresent())
            {
                par=OldPar.get();
               /* par.setLibelleC(new_Par.getLibelleC());
                par.setSalle(new_Par.getSalle());
                par.setEnseignant(new_Par.getEnseignant());*/
                ParRepository.save(par);
            }}
        @Override
        public List<Pers_morale_be> listPBE(){
            return ParRepository.findAll();
        }
        @Override
        public void removePBE(long CodeC){
            ParRepository.deleteById(CodeC);
        }
        @Override
        public Optional<Pers_morale_be> findPBE(long CodeC){
            return ParRepository.findById(CodeC);
        }
    }

