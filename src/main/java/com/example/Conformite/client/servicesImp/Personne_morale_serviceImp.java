package com.example.Conformite.client.servicesImp;

import com.example.Conformite.client.entities.Personne_morale;
import com.example.Conformite.client.repositories.Personne_morale_repository;
import com.example.Conformite.client.services.Personne_morale_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("personne_mrl_serviceImp")
public class Personne_morale_serviceImp implements Personne_morale_service {
    @Autowired
    Personne_morale_repository PM_Repository;


    @Override
    public void savePersonne_mrl (Personne_morale PM) {
        PM_Repository.save(PM);
    }
    @Override
    public void updatePersonne_mrl (Personne_morale PM) {
        PM_Repository.save(PM); }

    @Override
    public List<Personne_morale> listPersonne_mrl(){
        return PM_Repository.findAll();
    }
    @Override
    public void removePersonne_mrl(long code_clt ) {
        PM_Repository.deleteById(code_clt);
    }

    @Override
    public Optional<Personne_morale> findPersonne_mrl(long code_clt){
        return PM_Repository.findById(code_clt);
    }
}
