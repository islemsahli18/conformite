package com.example.Conformite.client.servicesImp;
import com.example.Conformite.client.entities.Nationalite;
import com.example.Conformite.client.entities.Personne_physique;
import com.example.Conformite.client.repositories.Nationalite_repository;
import com.example.Conformite.client.repositories.Personne_physique_repository;
import com.example.Conformite.client.services.Nationalite_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
@Service("NatServiceImpl")
public class Nationalite_serviceImp implements Nationalite_service {
    @Autowired
    Nationalite_repository NatRepository;
    @Autowired
    Personne_physique_repository PPRepository;
    @Override
    public void saveNat(Nationalite Nat) {
        NatRepository.save(Nat);
    }
    @Override
    public void updateNat(long codeC, Nationalite new_Nat) {
        Nationalite nat;
        Optional<Nationalite> OldNat=NatRepository.findById(codeC);
        if(OldNat.isPresent())
        {
            nat=OldNat.get();
               /* nat.setLibelleC(new_BF.getLibelleC());
                nat.setSalle(new_BF.getSalle());
                nat.setEnseignant(new_BF.getEnseignant());*/
            NatRepository.save(nat);
        }}
    @Override
    public  List<Nationalite> listNat(){
        return NatRepository.findAll();
    }
    @Override
    public  void removeNat(long CodeC){
        NatRepository.deleteById(CodeC);
    }
    @Override
    public Optional<Nationalite> findNat(long CodeC){
        return NatRepository.findById(CodeC);
    }

    @Override
    public Nationalite enrollNationaliteToPerson (
            Long nationaliteID ,
            Long personneID)
    {
        Nationalite nationalite = NatRepository.findById(nationaliteID).get();
        Personne_physique personne=PPRepository.findById(personneID).get();

        return NatRepository.save(nationalite);
    }
}
