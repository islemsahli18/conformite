package com.example.Conformite.client.servicesImp;

import com.example.Conformite.client.entities.Pers_nat;
import com.example.Conformite.client.repositories.Pers_nat_repository;
import com.example.Conformite.client.services.Pers_nat_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service("Pers_nat_serviceImp")
public class Pers_nat_serviceImp implements Pers_nat_service {

    @Autowired
    Pers_nat_repository ParRepository;
    @Override
    public void savePN(Pers_nat Par) {
        ParRepository.save(Par);
    }
    @Override
    public void updatePN(long codeC, Pers_nat new_Par) {
        Pers_nat par;
        Optional<Pers_nat> OldPar= ParRepository.findById(codeC);
        if(OldPar.isPresent())
        {
            par=OldPar.get();
               /* par.setLibelleC(new_Par.getLibelleC());
                par.setSalle(new_Par.getSalle());
                par.setEnseignant(new_Par.getEnseignant());*/
            ParRepository.save(par);
        }}
    @Override
    public List<Pers_nat> listPN(){
        return ParRepository.findAll();
    }
    @Override
    public void removePN(long CodeC){
        ParRepository.deleteById(CodeC);
    }
    @Override
    public Optional<Pers_nat> findPN(long CodeC){
        return ParRepository.findById(CodeC);
    }
}
