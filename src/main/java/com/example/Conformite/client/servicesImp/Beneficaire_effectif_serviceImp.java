package com.example.Conformite.client.servicesImp;
import com.example.Conformite.client.entities.Beneficaire_effectif;
import com.example.Conformite.client.repositories.Beneficaire_effectif_repository;
import com.example.Conformite.client.services.Beneficaire_effectif_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service("BEServiceImpl")

public class Beneficaire_effectif_serviceImp implements Beneficaire_effectif_service {

    @Autowired
    Beneficaire_effectif_repository BFRepository;
    @Override
    public void saveBF(Beneficaire_effectif BF) {
        BFRepository.save(BF);
    }
    @Override
    public void updateBF(long codeC, Beneficaire_effectif new_BF) {
        Beneficaire_effectif BF1;
        Optional<Beneficaire_effectif> OldBF=BFRepository.findById(codeC);
        if(OldBF.isPresent())
        {
            BF1=OldBF.get();
               /* BF1.setLibelleC(new_BF.getLibelleC());
                BF1.setSalle(new_BF.getSalle());
                BF1.setEnseignant(new_BF.getEnseignant());*/
            BFRepository.save(BF1);
        }
    }
    @Override
    public List<Beneficaire_effectif> listBF(){
        return BFRepository.findAll();
    }
    @Override
    public  void removeBF(long CodeC){
        BFRepository.deleteById(CodeC);
    }
    @Override
    public Optional<Beneficaire_effectif> findBF(long CodeC){
        return BFRepository.findById(CodeC);
    }
}
