package com.example.Conformite.client.servicesImp;

import com.example.Conformite.client.entities.Personne_physique;
import com.example.Conformite.client.repositories.Personne_physique_repository;
import com.example.Conformite.client.services.Personne_physique_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("personne_phy_serviceImp")
public class Personne_physique_serviceImp implements Personne_physique_service {
    @Autowired
    Personne_physique_repository PP_Repository;


    @Override
    public void savePersonne_phy(Personne_physique PP) {
        PP_Repository.save(PP);
    }

    @Override
    public void updatePersonne_phy(Personne_physique PP) {
        PP_Repository.save(PP);
    }

    @Override
    public List<Personne_physique> listPersonne_phy() {
        return PP_Repository.findAll();
    }

    @Override
    public void removePersonne_phy(long code_clt) {
        PP_Repository.deleteById(code_clt);
    }

    @Override
    public Optional<Personne_physique> findPersonne_phy(long code_clt) {
        return PP_Repository.findById(code_clt);
    }
}
