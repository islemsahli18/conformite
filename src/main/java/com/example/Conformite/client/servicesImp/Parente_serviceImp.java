package com.example.Conformite.client.servicesImp;

import com.example.Conformite.client.entities.Parente;
import com.example.Conformite.client.repositories.Parente_repository;
import com.example.Conformite.client.services.Parente_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
@Service("PARServiceImpl")
public class Parente_serviceImp implements Parente_service {
    @Autowired
    Parente_repository ParRepository;
    @Override
    public void savePar(Parente Par) {
        ParRepository.save(Par);
    }
    @Override
    public void updatePar(long codeC, Parente new_Par) {
        Parente par;
        Optional<Parente> OldPar= ParRepository.findById(codeC);
        if(OldPar.isPresent())
        {
            par=OldPar.get();
               /* par.setLibelleC(new_Par.getLibelleC());
                par.setSalle(new_Par.getSalle());
                par.setEnseignant(new_Par.getEnseignant());*/
            ParRepository.save(par);
        }}
    @Override
    public List<Parente> listPar(){
        return ParRepository.findAll();
    }
    @Override
    public void removePar(long CodeC){
        ParRepository.deleteById(CodeC);
    }
    @Override
    public Optional<Parente> findPar(long CodeC){
        return ParRepository.findById(CodeC);
    }
}
