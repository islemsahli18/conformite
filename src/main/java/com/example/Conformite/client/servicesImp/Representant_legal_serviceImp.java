package com.example.Conformite.client.servicesImp;


import com.example.Conformite.client.entities.Representant_legal;
import com.example.Conformite.client.repositories.Representant_legal_repository;
import com.example.Conformite.client.services.Representant_legal_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service("representant_legal_serviceImp")
public class Representant_legal_serviceImp implements Representant_legal_service {
    @Autowired
    Representant_legal_repository RL_Repository;


    @Override
    public void saveRepresentant_legal(Representant_legal RL) {
        RL_Repository.save(RL);
    }

    @Override
    public void updateRepresentant_legal(Representant_legal RL) {
        RL_Repository.save(RL);
    }

    @Override
    public List<Representant_legal> listRepresentant_legal() {
        return RL_Repository.findAll();
    }

    @Override
    public void removeRepresentant_legal(long code_clt) {
        RL_Repository.deleteById(code_clt);
    }

    @Override
    public Optional<Representant_legal> findRepresentant_legal(long code_clt) {
        return RL_Repository.findById(code_clt);
    }
}
