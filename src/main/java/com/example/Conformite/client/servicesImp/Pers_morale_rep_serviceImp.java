package com.example.Conformite.client.servicesImp;

import com.example.Conformite.client.entities.Pers_morale_rep;

import com.example.Conformite.client.repositories.Pers_rep_repository;

import com.example.Conformite.client.services.Pers_morale_rep_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service("Pers_rep_serviceImp")
public class Pers_morale_rep_serviceImp implements Pers_morale_rep_service {
        @Autowired
        Pers_rep_repository ParRepository;
        @Override
        public void savePR(Pers_morale_rep Par) {
            ParRepository.save(Par);
        }
        @Override
        public void updatePR(long codeC, Pers_morale_rep new_Par) {
            Pers_morale_rep par;
            Optional<Pers_morale_rep> OldPar= ParRepository.findById(codeC);
            if(OldPar.isPresent())
            {
                par=OldPar.get();
               /* par.setLibelleC(new_Par.getLibelleC());
                par.setSalle(new_Par.getSalle());
                par.setEnseignant(new_Par.getEnseignant());*/
                ParRepository.save(par);
            }}
        @Override
        public List<Pers_morale_rep> listPR(){
            return ParRepository.findAll();
        }
        @Override
        public void removePR(long CodeC){
            ParRepository.deleteById(CodeC);
        }
        @Override
        public Optional<Pers_morale_rep> findPR(long CodeC){
            return ParRepository.findById(CodeC);
        }
    }




