package com.example.Conformite.client.servicesImp;

import com.example.Conformite.client.entities.Pers_par;
import com.example.Conformite.client.repositories.Pers_par_repository;
import com.example.Conformite.client.services.Pers_par_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service("Pers_par_serviceImp")
public class Pers_par_serviceImp implements Pers_par_service {



        @Autowired
        Pers_par_repository ParRepository;
        @Override
        public void savePS(Pers_par Par) {
            ParRepository.save(Par);
        }
        @Override
        public void updatePS(long codeC,Pers_par new_Par) {
            Pers_par par;
            Optional<Pers_par> OldPar= ParRepository.findById(codeC);
            if(OldPar.isPresent())
            {
                par=OldPar.get();
               /* par.setLibelleC(new_Par.getLibelleC());
                par.setSalle(new_Par.getSalle());
                par.setEnseignant(new_Par.getEnseignant());*/
                ParRepository.save(par);
            }}
        @Override
        public List<Pers_par> listPS(){
            return ParRepository.findAll();
        }
        @Override
        public void removePS(long CodeC){
            ParRepository.deleteById(CodeC);
        }
        @Override
        public Optional<Pers_par> findPS(long CodeC){
            return ParRepository.findById(CodeC);
        }
    }


