package com.example.Conformite.client.servicesImp;



import com.example.Conformite.client.entities.Revenu;
import com.example.Conformite.client.repositories.Revenu_repository;
import com.example.Conformite.client.services.Revenu_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service("revenu_serviceImp")
public class Revenu_serviceImp implements Revenu_service {
    @Autowired
    Revenu_repository R_Repository;

    @Override
    public void saveRevenu(Revenu R) {
        R_Repository.save(R);
    }

    @Override
    public void updateRevenu(Revenu R) {
        R_Repository.save(R);
    }

    @Override
    public List<Revenu> listRevenu() {
        return R_Repository.findAll();
    }

    @Override
    public void removeRevenu(long ID_R) {
        R_Repository.deleteById(ID_R);
    }

    @Override
    public Optional<Revenu> findRevenu(long ID_R) {
        return R_Repository.findById(ID_R);
    }
    }

