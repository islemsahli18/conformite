package com.example.Conformite.client.services;

import com.example.Conformite.client.entities.Pers_par;


import java.util.List;
import java.util.Optional;

public interface Pers_par_service {

    void savePS(Pers_par Par);
    void updatePS(long codeC,Pers_par Par);
    List<Pers_par> listPS();
    void removePS(long CodeC);
    public Optional<Pers_par > findPS(long CodeC);
}
