package com.example.Conformite.client.services;


import com.example.Conformite.client.entities.Pers_morale_rep;

import java.util.List;
import java.util.Optional;

public interface Pers_morale_rep_service {
    void savePR(Pers_morale_rep Par);
    void updatePR(long codeC, Pers_morale_rep Par);
    List<Pers_morale_rep> listPR();
    void removePR(long CodeC);
    public Optional<Pers_morale_rep> findPR(long CodeC);

}
