package com.example.Conformite.client.services;

import com.example.Conformite.client.entities.Personne_morale;
import java.util.List;
import java.util.Optional;

public interface Personne_morale_service {
    void savePersonne_mrl(Personne_morale PM);
    void updatePersonne_mrl (Personne_morale PM);
    List<Personne_morale> listPersonne_mrl();
    void removePersonne_mrl(long code_clt);
    public Optional<Personne_morale> findPersonne_mrl(long code_clt);
}
