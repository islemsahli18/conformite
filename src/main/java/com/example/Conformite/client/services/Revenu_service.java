package com.example.Conformite.client.services;

import com.example.Conformite.client.entities.Revenu;
import java.util.List;
import java.util.Optional;

public interface Revenu_service {

    void saveRevenu(Revenu R);
    void updateRevenu(Revenu R);
    List<Revenu> listRevenu();
    void removeRevenu(long ID_R);
    public Optional<Revenu> findRevenu(long ID_R);
}
