package com.example.Conformite.client.services;


import com.example.Conformite.client.entities.Personne_physique;
import java.util.List;
import java.util.Optional;

public interface Personne_physique_service {

    void savePersonne_phy(Personne_physique PS);
    void updatePersonne_phy (Personne_physique PS);
    List<Personne_physique> listPersonne_phy();
    void removePersonne_phy(long code_clt);
    public Optional<Personne_physique> findPersonne_phy(long code_clt);
}
