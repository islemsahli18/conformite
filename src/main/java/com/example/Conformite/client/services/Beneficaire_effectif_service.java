package com.example.Conformite.client.services;

import com.example.Conformite.client.entities.Beneficaire_effectif;

import java.util.List;
import java.util.Optional;

public interface Beneficaire_effectif_service {
    void saveBF(Beneficaire_effectif BF);
    void updateBF(long codeC, Beneficaire_effectif BF);
    List<Beneficaire_effectif> listBF();
    void removeBF(long CodeC);
    public Optional<Beneficaire_effectif> findBF(long CodeC);
}
