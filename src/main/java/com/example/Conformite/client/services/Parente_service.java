package com.example.Conformite.client.services;

import com.example.Conformite.client.entities.Parente;
import java.util.List;
import java.util.Optional;

public interface Parente_service {
    void savePar(Parente Par);
    void updatePar(long codeC, Parente Par);
    List<Parente> listPar();
    void removePar(long CodeC);
    public Optional<Parente> findPar(long CodeC);
}
