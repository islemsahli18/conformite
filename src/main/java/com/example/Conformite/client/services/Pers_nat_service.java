package com.example.Conformite.client.services;


import com.example.Conformite.client.entities.Pers_nat;

import java.util.List;
import java.util.Optional;

public interface Pers_nat_service {

    void savePN(Pers_nat Par);
    void updatePN(long codeC, Pers_nat Par);
    List<Pers_nat> listPN();
    void removePN(long CodeC);
    public Optional<Pers_nat> findPN(long CodeC);
}
