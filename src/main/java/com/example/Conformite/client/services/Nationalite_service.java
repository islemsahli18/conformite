package com.example.Conformite.client.services;
import com.example.Conformite.client.entities.Nationalite;
import java.util.List;
import java.util.Optional;

public interface Nationalite_service {
    void saveNat(Nationalite Nat);
    void updateNat(long codeC, Nationalite Nat);
    List<Nationalite> listNat();
    void removeNat(long CodeC);
    Optional<Nationalite> findNat(long CodeC);
    Nationalite enrollNationaliteToPerson (Long nationaliteID , Long personneID);
}
