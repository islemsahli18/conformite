package com.example.Conformite.client.services;

import com.example.Conformite.client.entities.Representant_legal;
import java.util.List;
import java.util.Optional;

public interface Representant_legal_service {

    void saveRepresentant_legal(Representant_legal RL);
    void updateRepresentant_legal (Representant_legal RL);
    List<Representant_legal> listRepresentant_legal();
    void removeRepresentant_legal(long code_clt);
    public Optional<Representant_legal> findRepresentant_legal(long code_clt);
}
