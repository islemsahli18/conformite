package com.example.Conformite.client.services;

import com.example.Conformite.client.entities.Pers_morale_be;

import java.util.List;
import java.util.Optional;

public interface Pers_morale_be_service {
    void savePBE(Pers_morale_be Par);
    void updatePBE(long codeC, Pers_morale_be Par);
    List<Pers_morale_be> listPBE();
    void removePBE(long CodeC);
    public Optional<Pers_morale_be> findPBE(long CodeC);

}
