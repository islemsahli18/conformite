package com.example.Conformite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class ConformiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConformiteApplication.class, args);
	}


}
