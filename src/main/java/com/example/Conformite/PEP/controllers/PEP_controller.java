package com.example.Conformite.PEP.controllers;

import com.example.Conformite.PEP.entities.PEP;
import com.example.Conformite.PEP.services.PEP_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value="/PEP",method= {RequestMethod.GET})
public class PEP_controller {

        @Autowired
        PEP_service BEService;
        @PostMapping("/Create")
        public String createCours(@Validated @RequestBody PEP BE) {
            BEService.savePEP(BE);
            return "PEPcree";
        }
        @GetMapping("/GetAll")
        public List<PEP> getAll(){
            return BEService.listPEP();
        }
        @GetMapping("/GetBE/{id}")
        public Optional<PEP> findCours(@PathVariable Long id){
      /*Optional<Cours> cours=coursService.findCours(id);
      if(cours==null) throw new
      CoursIntrouvableException("Le cours avec le code "+id+"est introuvable");*/
            return BEService.findPEP(id);
        }
        @PutMapping("/UpdateBE/{id}")
        public String UpdateCours(@Validated @RequestBody PEP cours,@PathVariable Long id){
            BEService.updatePEP(id,cours);
            return"La mise a jour a ete faite avec succees";
        }
        @DeleteMapping(value="/DeleteBE/{id}")
        public String deleteCours(@PathVariable String id){
            BEService.removePEP(Long.parseLong(id));
            return "BE supprimee avec succee";
        }
}
