package com.example.Conformite.PEP.entities;


import com.example.Conformite.client.entities.Contact;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class PEP {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private  Long Code_pep;
    private String fonction_pep;

    private String pays_pep;

    private Date date_debut;
    private Date date_fin;

    //Relations
    @OneToOne
    @JoinColumn(name="Code_client")
    private Contact contact ;


    public PEP() {
    }

    public PEP(Long code_pep, String fonction_pep, String pays_pep, Date date_debut, Date date_fin) {
        Code_pep = code_pep;
        this.fonction_pep = fonction_pep;
        this.pays_pep = pays_pep;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
    }

    public Long getCode_pep() {
        return Code_pep;
    }

    public void setCode_pep(Long code_pep) {
        Code_pep = code_pep;
    }

    public String getFonction_pep() {
        return fonction_pep;
    }

    public void setFonction_pep(String fonction_pep) {
        this.fonction_pep = fonction_pep;
    }

    public String getPays_pep() {
        return pays_pep;
    }

    public void setPays_pep(String pays_pep) {
        this.pays_pep = pays_pep;
    }

    public Date getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(Date date_debut) {
        this.date_debut = date_debut;
    }

    public Date getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(Date date_fin) {
        this.date_fin = date_fin;
    }


    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
