package com.example.Conformite.PEP.services;

import com.example.Conformite.PEP.entities.PEP;
import java.util.List;
import java.util.Optional;

public interface PEP_service {

    void savePEP(PEP Nat);
    void updatePEP(long codeC, PEP Nat);
    List<PEP > listPEP();
    void removePEP(long CodeC);
    Optional<PEP> findPEP(long CodeC);

}
