package com.example.Conformite.PEP.servicesImp;

import com.example.Conformite.PEP.entities.PEP;
import com.example.Conformite.PEP.repositories.PEP_repository;
import com.example.Conformite.PEP.services.PEP_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service("PEP_serviceImpl")
public class PEP_serviceImp implements PEP_service {

        @Autowired
        PEP_repository ParRepository;
        @Override
        public void savePEP(PEP Par) {
            ParRepository.save(Par);
        }
        @Override
        public void updatePEP(long codeC,PEP new_Par) {
            PEP par;
            Optional<PEP> OldPar= ParRepository.findById(codeC);
            if(OldPar.isPresent())
            {
                par=OldPar.get();
               /* par.setLibelleC(new_Par.getLibelleC());
                par.setSalle(new_Par.getSalle());
                par.setEnseignant(new_Par.getEnseignant());*/
                ParRepository.save(par);
            }}
        @Override
        public List<PEP > listPEP(){
            return ParRepository.findAll();
        }
        @Override
        public void removePEP(long CodeC){
            ParRepository.deleteById(CodeC);
        }
        @Override
        public Optional<PEP > findPEP(long CodeC){
            return ParRepository.findById(CodeC);
        }

    }
