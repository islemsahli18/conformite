package com.example.Conformite.PEP.repositories;

import com.example.Conformite.PEP.entities.PEP;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PEP_repository extends JpaRepository<PEP,Long> {
}
